'use strict';

var dao = require('../dao/DAO');
  

  
  exports.create_a_OOCPA_Request = function(req, res) {
    console.log('OOCPA_Request ');
   // dao.connection();
    let body = '';
    if (req.query !== {}) 
    {
    req.on('data', chunk => {
        body += chunk.toString();
        
    });
    req.on('end', () => {
       
        res.end('ok');
      });
      res.json(req.body);
    // console.log(req.body);
     dao.insert(req);
    }
    else
    {
      res.json("Invalid data");
    }
  };

  exports.save_a_OOCPA_Request = function(req, res) {
    console.log('Save_OOCPA_Request ');
   // dao.connection();
    let body = '';
    if (req.query !== {}) 
    {
    req.on('data', chunk => {
        body += chunk.toString();
        
    });
    req.on('end', () => {
       
        res.end('ok');
      });
      res.json(req.body);
    // console.log(req.body);
     dao.save(req);
    }
    else
    {
      res.json("Invalid data");
    }
  };
  
  exports.get_a_OOCPA_Request = function(req, res) {
    console.log('OOCPA_Search');
     
     dao.getProviderById(req,res);
    
  };

  exports.get_all_OOCPA_Request = function(req, res) {
    console.log('OOCPA_Search');
     
     dao.getProviders(req,res);
    
  };
  