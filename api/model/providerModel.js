class Provider {

  constructor() {

    this.stakeholderNo = null;
    this.firstName = null;
    this.lastName = null;
    this.primaryNumber = null;
    this.extension = null;
    this.workNumber = null;
    this.fax = null;
    this.email = null;
  }

  getSQLValues() {
    return ["nextval('public.hibernate_sequence')", "'" + this.getStakeholder() + "'", "'" + this.getTimeStamp() + "'", "'" + this.getEmail() + "'", "'" + this.getFax() + "'", "'" + this.getFName() + "'", "'" + this.getLName() + "'", "'" + this.getPrimaryNumber() + "'", "'" + this.getTimeStamp() + "'", "'" + this.getExtension() + "'"];
  }

  getSQL() {
    return 'INSERT INTO refering_physician_info(id, billing_number, created_timestamp, email, fax, first_name,last_name, phone, last_update_timestamp,extension)  VALUES (' + this.getSQLValues() + ');'
  }
  getStakeholder() { return this.stakeholderNo; }
  getTimeStamp() {return  new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')}
  getFName() { return this.firstName; }
  getLName() { return this.lastName; }
  getPrimaryNumber() { return this.primaryNumber; }
  getExtension() { return this.extension; }
  getWorkNumber() { return this.workNumber; }
  getFax() { return this.fax; }
  getEmail() { return this.fax; }

  initModel(data) {

    this.stakeholderNo = data.stakeholderNo;
    this.firstName = data.firstName;
    this.lastName = data.lastName;
    this.primaryNumber = data.primaryNumber;
    this.extension = data.extension;
    this.workNumber = data.workNumber;
    this.fax = data.fax;
    this.email = data.email;

  }
}
module.exports = Provider;
