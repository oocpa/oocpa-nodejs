  class Treatment {

    constructor() {
  
      this.Diagnosis = null;
      this.Code = null;
      this.ServiceType = null;
      this.HosptialStay = null;
      this.AdmissionDate = null;
      this.OOCConsult = null;
      this.SurgeryDate = null;
      this.ProposedTreatment = null;
      this.OutOfCountry = null;
      this.PreviousRequest = null;
      this.CardiacCare = null;
    }
  
    getSQLValues() {
      return ["nextval('public.hibernate_sequence')", "'" + this.getStakeholder() + "'", "'" + this.getTimeStamp() + "'", "'" + this.getEmail() + "'", "'" + this.getFax() + "'", "'" + this.getFName() + "'", "'" + this.getLName() + "'", "'" + this.getPrimaryNumber() + "'", "'" + this.getTimeStamp() + "'", "'" + this.getExtension() + "'"];
    }
  
    getSQL() {
      return 'INSERT INTO refering_physician_info(id, billing_number, created_timestamp, email, fax, first_name,last_name, phone, last_update_timestamp,extension)  VALUES (' + this.getSQLValues() + ');'
    }
    getDiagnosis() { return this.Diagnosis; }
    getTimeStamp() {return  new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')}
    getCode() { return this.Code; }
    getServiceType() { return this.ServiceType; }
    getHosptialStay() { return this.HosptialStay; }
    getAdmissionDate() { return this.AdmissionDate; }
    getOOCConsult() { return this.OOCConsult; }
    getSurgeryDate() { return this.SurgeryDate; }
    getProposedTreatment() { return this.ProposedTreatment; }
    getOutOfCountry() { return this.OutOfCountry; }
    getPreviousRequest() { return this.PreviousRequest; }
    getCardiacCare() { return this.CardiacCare; }
  
    initModel(data) {
  
        this.Diagnosis = data.Diagnosis;
        this.Code = data.Code;
        this.ServiceType = data.ServiceType;
        this.HosptialStay = data.HosptialStay;
        this.AdmissionDate = data.AdmissionDate;
        this.OOCConsult = data.OOCConsult;
        this.SurgeryDate = data.SurgeryDate;
        this.ProposedTreatment = data.ProposedTreatment;
        this.OutOfCountry = data.OutOfCountry;
        this.PreviousRequest = data.PreviousRequest;
        this.CardiacCare = data.CardiacCare;
    
  
    }
  }
  module.exports = Treatment;
  