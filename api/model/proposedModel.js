class Proposed {

    constructor() {


        this.facility = null;
        this.address = null;
        this.city = null;
        this.province = null;
        this.primaryNumber = null;
        this.workNumber = null;
        this.fax = null;
        this.email = null;
        this.contact = null;
        this.contactLastName = null;
        this.contactFirstName = null;

    }

    getSQLValues() {
      //  return ["nextval('public.hibernate_sequence')", "'" + this.getTimeStamp() + "'", "'" + this.getDOB() + "'", "'" + this.getFirstName() + "'", "'" + this.getSex() + "'", "'" + this.getHealthNumber() + "'", "'" + this.getLastName() + "'", "'" + this.getGuardFirstName() + "'", "'" + this.getGuardLastName() + "'", "'" + this.getRelationship() + "'", "'" + this.getPrimaryNumber() + "'", "'" + this.getTimeStamp() + "'", "'" + this.getVersionCode() + "'"];
    }

    getSQL() {
     //   return 'INSERT INTO public.patient_info(id, created_timestamp, date_of_birth, first_name, gender, health_number,last_name, legal_guardianfn, legal_guardianln, legal_signer, phone, last_update_timestamp,version) VALUES (' + this.getSQLValues() + ');'
    }

    getTimeStamp() { return new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '') }
    getFacility() {return this.facility}
     getAddress() {return this.address}
    getCity() {return this.city}
    getProvince() {return this.province}
    getPrimaryNumber() {return this.primaryNumber}
    getFax() {return this.fax}
    getEmail() {return this.email}
    getContact() {return this.contact}
    getContactLastName() {return this.contactLastName}
    getContactFirstName() {return this.contactFirstName}
    
    initModel(data) {


        this.facility = data.facility;
        this.address = data.address;
        this.city = data.city;
        this.province = data.province;
        this.primaryNumber = data.primaryNumber;
        this.fax = data.fax;
        this.email = data.email;
        this.contact = data.contact;
        this.contactLastName = data.contactLastName;
        this.contactFirstName = data.contactFirstName;




    }
}
module.exports = Proposed;
