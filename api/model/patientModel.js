class Patient {

    constructor() {

        this.healthCardNumber = null;
        this.versionCode = null;
        this.firstName = null;
        this.intial = null;
        this.lastName = null;
        this.dateOfBirth = null;
        this.sex = null;
        this.city = null;
        this.province = null;
        this.postal = null;
        this.primaryNumber = null;
        this.workNumber = null;
        this.extension = null;
        this.relationship = null;
        this.guardLastName = null;
        this.guardFirstName = null;

    }

    getSQLValues() {
        return ["nextval('public.hibernate_sequence')", "'" + this.getTimeStamp() + "'", "'" + this.getDOB() + "'", "'" + this.getFirstName() + "'", "'" + this.getSex() + "'", "'" + this.getHealthNumber() + "'", "'" + this.getLastName() + "'", "'" + this.getGuardFirstName() + "'", "'" + this.getGuardLastName() + "'", "'" + this.getRelationship() + "'", "'" + this.getPrimaryNumber() + "'", "'" + this.getTimeStamp() + "'", "'" + this.getVersionCode() + "'"];
    }

    getSQL() {
        return 'INSERT INTO public.patient_info(id, created_timestamp, date_of_birth, first_name, gender, health_number,last_name, legal_guardianfn, legal_guardianln, legal_signer, phone, last_update_timestamp,version) VALUES (' + this.getSQLValues() + ');'
    }

    getTimeStamp() { return new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '') }
    getHealthNumber() { return this.healthCardNumber }
    getVersionCode() { return this.versionCode }
    getFirstName() { return this.firstName }
    getLastName() { return this.lastName }
    getInitial() { return this.intial }
    getDOB() { return this.dateOfBirth }
    getSex() { return this.sex }
    getCity() { return this.city }
    getProvince() { return this.province }
    getPostal() { return this.postal }
    getPrimaryNumber() { return this.primaryNumber }
    getWorkNumber() { return this.workNumber }
    getExtension() { return this.extension }
    getRelationship() { return this.relationship }
    getGuardLastName() { return this.guardLastName }
    getGuardFirstName() { return this.guardFirstName }

    initModel(data) {



        this.healthCardNumber = data.healthCardNumber;
        this.versionCode = data.versionCode;
        this.firstName = data.firstName;
        this.intial = data.intial;
        this.lastName = data.lastName;
        this.dateOfBirth = data.dateOfBirth;
        this.sex = data.sex;
        this.city = data.city;
        this.province = data.province;
        this.postal = data.postal;
        this.primaryNumber = data.primaryNumber;
        this.workNumber = data.workNumber;
        this.extension = data.extension;
        this.relationship = data.relationship;
        this.guardLastName = data.guardLastName;
        this.guardFirstName = data.guardFirstName;


    }
}
module.exports = Patient;
