var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000,
  
  bodyParser = require('body-parser');

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


var routes = require('./api/routes/index'); //importing route
routes(app); //register the route


app.listen(port);


console.log('OOCPA NodeJS RestFUL started: ' + port);




